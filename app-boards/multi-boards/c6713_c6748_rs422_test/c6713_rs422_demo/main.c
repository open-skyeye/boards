/*
 * main.c
 */
#include <stdio.h>
#include <math.h>

/*******************************************************/
/*  printf��������                                      */
/*******************************************************/

#define SKYEYE 1
void uart_putc (const char c)
{
		if (SKYEYE)
			*(char *)0x1b7e070 = c; /* write char to Transmit Buffer Register */
		else
			putc(c, stdout);
		return;
}

void uart_puts (const char *s)
{
    while (*s)
    {
	uart_putc (*s++);
    }
    return;
}


int my_strlen(char *str)
{
	int i;
	for(i = 0; str[i] != '\0'; i++);
	return i;
}
char *myitoa(int n, int hex)
{
    int num, i = 0, k = 0;
    static char str[100];
    char temp;
    if(n < 0)
    {
        str[i++] = '-';
        n = -1 * n;
    }
    do
    {
    	 num = n;
    	 n = n / hex;
    	 if (num%hex > 9)
    	     str[i+k++] = 'a' + (num%hex - 0xa);
    	 else
    	     str[i + k++] = '0' + num % hex;
    }while(n);
    str[i + k] = '\0';

    k = my_strlen(str) - 1;

    for(;i < k; i++, k--)
    {
        temp = str[i];
        str[i] = str[k];
        str[k] = temp;
    }
    return str;
}
unsigned square(unsigned val, unsigned num){
	int i;
	unsigned ret = val;
	for(i = 0; i < num - 1; i++){
		ret = ret*val;
	}
	return ret;
}
char * ftoa(double val, int prec)
{
	int sig = 0, index = 0, p, i, j;
	static char s[100] = {'\0'};
	char temp_ch;
	long val_long, num;
	double val_temp;
	if(val < 0){
		s[index++] = '-';
		val_long = ((long)val) * -1;
		val_temp = val * -1;
		p = 1;
	}else{
		val_long = (long)val;
		val_temp = val;
		p = 0;
	}

	do
    	{
        	num = val_long;
        	val_long = val_long / 10;
        	s[index++] = '0' + num % 10;
    	}while(val_long);
	for(i = p, j = index - 1; i < j; i++, j--){
		temp_ch = s[i];
		s[i] = s[j];
		s[j] = temp_ch;
	}
	s[index++] = '.';
	p = index;
	float v = (val_temp - (long)val_temp) * square(10, prec);
	long val_float = (long)v;
	for(i = 0; i < prec; i++){
		num = val_float;
		val_float = val_float/10;
        	s[index++] = '0' + num % 10;
	}
	for(i = p, j = index - 1; i < j; i++, j--){
		temp_ch = s[i];
		s[i] = s[j];
		s[j] = temp_ch;
	}
	s[index] = '\0';
	return s;
}

void uart_printf(const char *format, ...)
{
    va_list ap;
    char c;
    va_start(ap ,format);
    while((c = *format++) > 0)
    {
        if(c == '%')
        {
        switch(c = *format++){
            case 'c':{
                         char ch = va_arg(ap, int);
                         uart_putc(ch);
                         break;
                     }
            case 's':{
                         char *p = va_arg(ap, char*);
                         uart_puts(p);
                         break;
                     }
            case '%':
                     {
                         uart_putc(c);
                         break;
                     }

            case 'd':
                     {
                         int num = va_arg(ap, int);
                         char *p = myitoa(num, 10);
                         uart_puts(p);
                         break;
                     }
            case 'x':
                     {
                         int num = va_arg(ap, int);
                         char *p = myitoa(num, 16);
                         uart_puts(p);
                         break;
                     }

             case	'f':
                     {
						double fdata = va_arg(ap, double);
						char *str = ftoa(fdata, 6);
                        uart_puts(str);
						break;
		             }
            default:
                     uart_putc(c);
                     break;
            }
        }
        else if(c == '\\')
        {
        	switch(c = *format++){
            case 'n':{
						uart_putc('\r');
                         break;
                     }

            default:
            	uart_putc(c);
            	break;
            }
        }
        else
            uart_putc(c);
    }
    va_end(ap);
}

#define BASE_ADDR	           0x400000

#define SEND_DATA_OFFSET              0x0
#define SEND_DATA_LENGTH_OFFSET       0x4
#define RECV_DATA_OFFSET              0x8
#define RECV_DATA_LENGTH_OFFSET       0xc
#define BRSR_OFFSET 0x10

#define CMD_OFFSET         0x14
#define STATUS_OFFSET      0x18


char read_32regs(unsigned int addr){
	return *(volatile unsigned int *)addr;
}

void write_32regs(unsigned int addr, char value){
	*(volatile unsigned int *)addr = value;
	return;
}
void write_regs(unsigned int addr_offset, int value){
	write_32regs(BASE_ADDR + addr_offset, value);
	return;
}
int read_regs(unsigned int addr_offset){
	int value = read_32regs(BASE_ADDR + addr_offset);
	return value;
}

void Demo_transmit_data(char *buf, int length){
	int i;
	uart_printf("C6713 Start Send data : ");
	for (i = 0;i < length;i++){
		write_regs(SEND_DATA_OFFSET, buf[i]);
		uart_printf("0x%x ", buf[i]);
	}
	uart_printf("\n");
	write_regs(CMD_OFFSET, 0x1); //start send data cmd
}
void delay(){
	static int i = 1000000;
	while(i--)
	;
	i = 1000000;
}
#define Data_Length 12
int main(void) {
	int cmd = 0, i;
	char send_buf[Data_Length] = {0x7E, 0x11, 0x12, 0x13, 0x14,0x15,0x16,0x17,0x18,0x19,0x1a, 0x7E};
	uart_printf("        Start Test Rs422 Serial communication...\n");

	while(1){
		write_regs(SEND_DATA_LENGTH_OFFSET, Data_Length);
		Demo_transmit_data(send_buf, Data_Length);

		uart_printf("       C6713 Rs422 transmit data to other core ok...\n");
		delay();
	}
	return 0;
}
