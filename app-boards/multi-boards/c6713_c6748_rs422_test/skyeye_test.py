#coding:utf-8
import time
import os
import sys
from skyeye_autotest_command import *

class test():
	def __init__(self):
		testname="Demo.skyeye"
		test_branch='new_framework'
		enable=True
		run_system="all"
		self.ac=autotest_command(testname,enable,run_system,test_branch)

	def dev_init(self):
		self.ac.add_file_out_dev('c6713_core1_uart','log.txt')

	def test_flow(self):
		self.ac.run_script()
		self.dev_init()
		self.ac.run(4)
		self.ac.stop()
		self.ac.compare("log.txt",self.expect()[0],100)
		self.ac.reset()
		self.ac.output_result()
	def test_main(self):
		self.test_flow()
	def expect(self):
		expected_output=['''
  Start Test Rs422 Serial communication...
C6713 Start Send data : 0x7e 0x11 0x12 0x13 0x14 0x15 0x16 0x17 0x18 0x19 0x1a 0x7e
       C6713 Rs422 transmit data to other core ok...
C6713 Start Send data : 0x7e 0x11 0x12 0x13 0x14 0x15 0x16 0x17 0x18 0x19 0x1a 0x7e
       C6713 Rs422 transmit data to other core ok...
''']
		return expected_output



