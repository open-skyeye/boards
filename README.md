该仓库存放Open-SkyEye支持的各种板卡用例，目录结构如下：

```
├── app-boards   # 应用场景类板卡
├── core-boards  # 最小核心板
│   ├── arm
│   ├── c66x
│   ├── mips
│   └── ...
├── dev-boards   # 开发板
└── other-boadrs # 其他板卡
```

开发者可以根据这些板卡用例，搭建自己的目标机板卡。