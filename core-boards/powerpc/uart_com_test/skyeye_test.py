#coding:utf-8
import time
import os
import sys
from skyeye_autotest_command import *

class test():
	def __init__(self):
		testname="ppc.skyeye"
		test_branch='new_framework'
		enable=True
		run_system="all"
		self.ac=autotest_command(testname,enable,run_system,test_branch)

	def dev_init(self):
		self.ac.add_file_out_dev('ccsr_uart_0','log.txt')

	def test_flow(self):
		self.ac.run_script()
		self.dev_init()
		self.ac.run(20)
		self.ac.stop()
		self.ac.compare("log.txt",self.expect()[0],100)
		self.ac.reset()
		self.ac.output_result()
	def test_main(self):
		self.test_flow()
	def expect(self):
		expected_output=['''
Target Name: vxTarget 

Adding 5486 symbols for standalone.
 

 ]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]
 ]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]
 ]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]
      ]]]]]]]]]]]  ]]]]     ]]]]]]]]]]       ]]              ]]]]         (R)
 ]     ]]]]]]]]]  ]]]]]]     ]]]]]]]]       ]]               ]]]]            
 ]]     ]]]]]]]  ]]]]]]]]     ]]]]]] ]     ]]                ]]]]            
 ]]]     ]]]]] ]    ]]]  ]     ]]]] ]]]   ]]]]]]]]]  ]]]] ]] ]]]]  ]]   ]]]]]
 ]]]]     ]]]  ]]    ]  ]]]     ]] ]]]]] ]]]]]]   ]] ]]]]]]] ]]]] ]]   ]]]]  
 ]]]]]     ]  ]]]]     ]]]]]      ]]]]]]]] ]]]]   ]] ]]]]    ]]]]]]]    ]]]] 
 ]]]]]]      ]]]]]     ]]]]]]    ]  ]]]]]  ]]]]   ]] ]]]]    ]]]]]]]]    ]]]]
 ]]]]]]]    ]]]]]  ]    ]]]]]]  ]    ]]]   ]]]]   ]] ]]]]    ]]]] ]]]]    ]]]]
 ]]]]]]]]  ]]]]]  ]]]    ]]]]]]]      ]     ]]]]]]]  ]]]]    ]]]]  ]]]] ]]]]]
 ]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]
 ]]]]]]]]]]]]]]]]]]]]]]]]]]]]]       Development System
 ]]]]]]]]]]]]]]]]]]]]]]]]]]]]
 ]]]]]]]]]]]]]]]]]]]]]]]]]]]       VxWorks 6.9
 ]]]]]]]]]]]]]]]]]]]]]]]]]]       KERNEL: WIND version 2.13
 ]]]]]]]]]]]]]]]]]]]]]]]]]       Copyright Wind River Systems, Inc., 1984-2017

 CPU: Freescale P2020.  Processor #0.
 Memory Size: 0x80000000 (2048Mb).  BSP version 6.9/6.
 Created: Nov 16 2013 18:16:33
 ED&R Policy Mode: Deployed

-> 
'''
]
		return expected_output



