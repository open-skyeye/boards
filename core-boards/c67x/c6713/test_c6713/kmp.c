#include "output.h"

void GetNextVal(char *smallstr, char *next, int len)
{
    int k = -1;
    int j = 0;

    next[j] = -1;

    while (j < len - 1)
    {
	if (k == -1 || smallstr[k] == smallstr[j])
	{
	    next[++j] = ++k;
	}
	else
	{
	    k = next[k];
	}
    }
    return;
}


int KMP(char *bigstr, int blen, char * smallstr, int slen, char *next)
        {
            int i = 0;
            int j = 0;

            GetNextVal(smallstr, next, slen);

            while (i < blen && j < slen)
            {
                if (j == -1 || bigstr[i] == smallstr[j])
                {
                    i++;
                    j++;
                }
                else
                {
                    j = next[j];
                }
            }

            if (j == slen)
                return i - slen;

            return -1;
 }

void kmp(void)
{
    char *zstr = "ababcabababdc";
    int zlen = 13;

    char *mstr = "babdc";
    int mlen =  5;
    char next[5];
    int index;

    index = KMP(zstr, zlen, mstr, mlen, next);

    if (index == -1)
	Puts("No found\n");
    else
	/*Puts("Found index��%d", index);*/
    	Puts("Found\n");
}
