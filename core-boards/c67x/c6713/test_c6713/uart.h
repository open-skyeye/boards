#ifndef UART_H_
#define UART_H_

#include <file.h>
#include <stdio.h>

int uart_init();
void uart_putc(char c);
void uart_puts(char *s);
void uart_puthc(char c);
void uart_putf(double f);
void uart_putd(double d);
#endif
