importPackage(Packages.com.ti.debug.engine.scripting)
importPackage(Packages.com.ti.ccstudio.scripting.environment)
importPackage(Packages.java.lang)

var script = ScriptingEnvironment.instance()

script.traceBegin("BreakpointsTestLog.xml", "DefaultStylesheet.xsl")

script.setScriptTimeout(15000)

// Log everything
//script.traceSetConsoleLevel(TraceLevel.ALL)
//script.traceSetFileLevel(TraceLevel.ALL)

var deviceCCXMLFile = "./targetConfigs/TMS320C6474.ccxml";
var programToLoad = "./Debug/test_c6474.out";
var firstInsn = 0x108019e0;
var lastInsn = 0x2018;
var script = ScriptingEnvironment.instance();

var ds = script.getServer("DebugServer.1");

ds.setConfig(deviceCCXMLFile);

var cpus = ds.getListOfCPUs();
debugSession = ds.openSession(cpus[0]);

//debugSession.target.connect();
debugSession.memory.loadProgram( programToLoad );
var main = debugSession.symbol.getAddress("main");
var bp1 = debugSession.breakpoint.add(main);
var bp0 = debugSession.breakpoint.add(firstInsn);

debugSession.target.restart();
//debugSession.target.run();
debugSession.target.reset();
debugSession.memory.writeRegister("PC", firstInsn);

nPC = debugSession.expression.evaluate("PC")

var insn_num = 0
var PC = nPC

while(true/*debugSession.expression.evaluate("PC") != main*/)
{
	insn_num ++;
	debugSession.target.asmStep.into();
	nPC = debugSession.expression.evaluate("PC")
	if (PC == nPC) {
		continue
	} else {
		PC = nPC
	}
        script.traceWrite("######Current PC:" + Long.toHexString(nPC))
	script.traceWrite("    ILC:" + Long.toHexString(debugSession.expression.evaluate("ILC")))
	for (var i = 0; i < 32; i++)
		script.traceWrite("    A" + i + ":" + Long.toHexString(debugSession.expression.evaluate("A" + i)))
	for (var i = 0; i < 32; i++)
		script.traceWrite("    B" + i + ":" + Long.toHexString(debugSession.expression.evaluate("B" + i)))
}

// Using an expression - get the current value of the PC
nPC = debugSession.expression.evaluate("PC")

// Verify we halted at the correct address.  Use the Static Java method Long.toHexString() to convert the 
// result to a hex string when logging messages
if (nPC == main) {
        script.traceWrite("SUCCESS: Halted at correct location")
} else {
        script.traceWrite("FAIL: Expected halt at 0x" + Long.toHexString(main) + ", actually halted at 0x" + Long.toHexString(nPC))

        script.traceSetConsoleLevel(TraceLevel.INFO)
        script.traceWrite("TEST FAILED!")
        script.traceEnd()
        java.lang.System.exit(1);
}

// Run the program
//debugSession.target.run();

// All done
debugSession.terminate()
ds.stop()

script.traceSetConsoleLevel(TraceLevel.INFO)
script.traceWrite("TEST SUCCEEDED!")

// Stop logging and exit.
script.traceEnd()

