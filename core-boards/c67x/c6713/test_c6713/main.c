#include "uart.h"
#include "output.h"

void print_double_bytes(double d) {
	int i;
	char *p = (char*)&d;
	for (i = 0; i < 8; i++)
		uart_puthc(p[i]);
	uart_putc(' ');
}

void hundred_chicken(void)
{
	int x, y, z;
	for(x = 1; x < 20; x++)
		for(y = 1; y < 33; y++){
			z = 100 - x - y;
			if((z%3 == 0) && (x*5 + y*3 + z/3 == 100))
				//Printf("Rooster:%d,Hen:%d,Chick:%d\n",x, y ,z);
				Puts("Get Rooster, Hen, Click number ok\n");
		}
	return;
}
extern void kmp(void);
extern void five_total_wells(void);
extern void monkeys_eat_peach(void);
extern void lcs_main(void);
extern void kmp(void);
int main(void) {
	uart_init();
	Puts("------hundred chicken problem------\n\r");
	hundred_chicken();

	Puts("------five total well problem------\n\r");
	five_total_wells();

	//Puts("------monkeys eat peach problem------\n\r");
	//monkeys_eat_peach();

	Puts("------longest common subsequence problem------\n\r");
	lcs_main();

	Puts("------kmp problem------\n\r");
	kmp();

	return 0;
}
