#include <file.h>
#include <stdio.h>
#include "uart.h"

#define UART0_CON_ADDR	(long*)0x02950048
#define UART0_DATA_ADDR	(long*)0x02950040

static int uart_open(const char *path, unsigned flags, int llv_fd);
static int uart_close(int dev_fd);
static int uart_read(int dev_id, char *bu, unsigned count);
static int uart_write(int dev_fd, const char *buf, unsigned count);
static off_t uart_lseek(int dev_fd, off_t offset, int origin);
static int uart_unlink(const char *path);
static int uart_rename(const char *old_name, const char *new_name);

static int uart_open(const char *path, unsigned flags, int llv_fd) {
	return 1;
}

static int uart_close(int dev_fd) {
	return 0;
}

static int uart_read(int dev_id, char *bu, unsigned count) {
	return count;
}

static int uart_write(int dev_fd, const char *buf, unsigned count) {
	unsigned i;

	for (i = 0; i < count; i++)
		uart_putc(buf[i]);

	return count;
}

static off_t uart_lseek(int dev_fd, off_t offset, int origin) {
	return 0;
}
static int uart_unlink(const char *path) {
	return 0;
}

static int uart_rename(const char *old_name, const char *new_name) {
	return 0;
}
int uart_init() {
	volatile long *con_addr = UART0_CON_ADDR;
	*con_addr = 0x3;
#if 0
	add_device("uart", _MSA, uart_open, uart_close, uart_read, uart_write,
				uart_lseek, uart_unlink, uart_rename);

	if (!freopen("uart:stderrfile", "w", stderr)) {
		printf("Failed to freopen stderr\n");
		return -1;
	}

	if (!freopen("uart:stdoutfile", "w", stdout)) {
		printf("Failed to freopen stdout\n");
		return -2;
	}
#endif
	return 0;
}

#define SKYEYE 1
void uart_putc(char c) {
#if SKYEYE
	volatile long *paddr = UART0_DATA_ADDR;
	*paddr = c;
#else
	putc(c, stdout);
#endif
}

void uart_puts(char *s) {
	char *p = s;
	while (*p != '\0') {
		uart_putc(*p);
		p++;
	}
}

#define GET_HEX_CHAR(c) (c) > 9 ? 'a' + (c) - 10 : '0' + (c)
void uart_puthc(char c) {
	char u, l;
	u = (c >> 4) & 0xf;
	if (u > 9)
		u = 'a' + u - 10;
	else
		u = '0' + u;
	l = c & 0xf;
	if (l > 9)
		l = 'a' + l - 10;
	else
		l = '0' + l;

	uart_putc(u);
	uart_putc(l);
}

static void output_hex(char h){
	if( h > 9)
			uart_putc((h - 10) + 'a');
	else
			uart_putc(h + '0');
}
void uart_putf(double f){
		int a[32];
		unsigned long long t = *(unsigned long long*)&f;
		int exp = ((t >> 52) & 0x7FF) - 1023;
		unsigned long long frac = t & 0xFFFFFFFFFFFFF | 0x10000000000000;
		unsigned long long t1 = frac >> (64 - 12 - exp);
		unsigned long long t2 = (frac << (12 + exp)) >> (12 + exp);
		int i = 0;
		while (i < 64){
			char c = (t1 >> i) & 0xF;
			output_hex(c);
			i++;
		}
		uart_puts(" . ");
		i = 0;
		while (i < 64){
			char c = (t2 >> i) & 0xF;
			output_hex(c);
			i++;
		}

}

void uart_puti(long long integer, int padding) {
	char arr[10], i = 0, j;

	if (integer < 0) {
		uart_putc('-');
		integer = -integer;
	}

	while (integer >= 0 && i < 10) {
		arr[9 - i++] = integer % 10;
		integer /= 10;
		if (integer == 0 && padding <= 0)
			break;
	}

	for (j = 10 - i; j < 10; j++) {
		uart_putc(arr[j] + '0');
	}

}

void uart_putd(double d) {
	long long integer, fraction;

	if (d > 0.0)
		d += 0.00000000005;
	else
		d -= 0.00000000005;

	integer = (long long)d;
	fraction = (long long) ((d - integer) * 10000000000.0);
	if ((fraction % 10) >= 5) {

	}

	uart_puti(integer, 0);
	uart_putc('.');
	uart_puti(fraction, 10);
}

