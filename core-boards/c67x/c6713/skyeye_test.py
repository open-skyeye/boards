#coding:utf-8
import time
import os
import sys
from skyeye_autotest_command import *

class test():
	def __init__(self):
		testname="setup.skyeye"
		test_branch='new_framework'
		enable=True
		run_system="all"
		self.ac=autotest_command(testname,enable,run_system,test_branch)

	def dev_init(self):
		self.ac.add_file_out_dev('core_0_uart_0','log.txt')

	def test_flow(self):
		self.ac.run_script()
		self.dev_init()
		self.ac.run(4)
		self.ac.stop()
		self.ac.compare("log.txt",self.expect()[0],100)
		self.ac.reset()
		self.ac.output_result()
	def test_main(self):
		self.test_flow()
	def expect(self):
		expected_output=['''
------hundred chicken problem------
Get Rooster, Hen, Click number ok
Get Rooster, Hen, Click number ok
Get Rooster, Hen, Click number ok
------five total well problem------
Get a, b, c, d, e, h Value OK
------longest common subsequence problem------
Get position ok
Get position ok
Get position ok
Get position ok
------kmp problem------
Found
'''
]
		return expected_output



