#coding:utf-8
import time
import os
import sys
from skyeye_autotest_command import *

class test():
	def __init__(self):
		testname="s3c2410x.skyeye"
		test_branch='new_framework'
		enable=True
		run_system="all"
		self.ac=autotest_command(testname,enable,run_system,test_branch)

	def dev_init(self):
		#self.ac.add_file_out_dev(con_objname,file_name)
		self.ac.add_file_out_dev('core_0_uart_0','log.txt')
	def test_flow(self):
		self.ac.run_script()
		self.dev_init()
		self.ac.run(120)
		self.ac.stop()
		self.ac.compare("log.txt",self.expect()[0],99)
		self.ac.reset()
		self.ac.output_result()
	def test_main(self):
		self.test_flow()
	def expect(self):
		expected_output=['''
s3c24xx_serial_init(c02024ec,c020253c)

s3c24xx_serial_probe(c0203bc8, c020253c) 0

s3c24xx_serial_probe: initialising port c0202200...

s3c24xx_serial_init_port: port=c0202224, platdev=c0203bc8

s3c24xx_serial_init_port: c0202224 (hw 0)...

resource c01f49b8 (50000000..50003fff)

port: map=50000000, mem=f7000000, irq=70 (70,71), clock=1

s3c2410_serial_resetport: port=c0202224 (50000000), cfg=c0203b38

s3c24xx_serial_probe: adding port

s3c24xx_serial_console_setup: co=c02024a4 (0), 115200

s3c24xx_serial_console_setup: port=c0202224 (0)

s3c24xx_serial_console_setup: baud 115200

selecting clock c01f5680

config: 8bits/char

setting ulcon to 00000003, brddiv to 64, udivslot 00000000

uart: ulcon = 0x00000003, ucon = 0x00000385, ufcon = 0x00000011

[    0.000000] Linux version 3.2.72 (skx@skx-desktop) (gcc version 4.4.3 (ctng-1.6.1) ) #273 Sat Nov 21 16:53:28 CST 2015
[    0.000000] CPU: ARM920T [41129200] revision 0 (ARMv4T), cr=00003177
[    0.000000] CPU: VIVT data cache, VIVT instruction cache
[    0.000000] Machine: SMDK2410
[    0.000000] Memory policy: ECC disabled, Data cache writeback
[    0.000000] CPU S3C2410 (id 0x32410000)
[    0.000000] S3C24XX Clocks, Copyright 2004 Simtec Electronics
[    0.000000] S3C2410: core 120.000 MHz, memory 120.000 MHz, peripheral 120.000 MHz
[    0.000000] CLOCK: Slow mode (1.500 MHz), fast, MPLL on, UPLL on
[    0.000000] Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 8128
[    0.000000] Kernel command line: mem=32M console=ttySAC0,115200 init=/bin/sh root=/dev/ram0 initrd=0x30800000,0x00800000 ramdisk_size=2048 rw
[    0.000000] PID hash table entries: 128 (order: -3, 512 bytes)
[    0.000000] Dentry cache hash table entries: 4096 (order: 2, 16384 bytes)
[    0.000000] Inode-cache hash table entries: 2048 (order: 1, 8192 bytes)
[    0.000000] Memory: 32MB = 32MB total
[    0.000000] Memory: 22100k/22100k available, 10668k reserved, 0K highmem
[    0.000000] Virtual kernel memory layout:
[    0.000000]     vector  : 0xffff0000 - 0xffff1000   (   4 kB)
[    0.000000]     fixmap  : 0xfff00000 - 0xfffe0000   ( 896 kB)
[    0.000000]     vmalloc : 0xc2800000 - 0xf6000000   ( 824 MB)
[    0.000000]     lowmem  : 0xc0000000 - 0xc2000000   (  32 MB)
[    0.000000]     modules : 0xbf000000 - 0xc0000000   (  16 MB)
[    0.000000]       .text : 0xc0008000 - 0xc01d6000   (1848 kB)
[    0.000000]       .init : 0xc01d6000 - 0xc01ef000   ( 100 kB)
[    0.000000]       .data : 0xc01f0000 - 0xc02038e0   (  79 kB)
[    0.000000]        .bss : 0xc0203904 - 0xc021a2d0   (  91 kB)
[    0.000000] NR_IRQS:85
[    0.000000] Console: colour dummy device 80x30
[    0.005000] Calibrating delay loop... 29.59 BogoMIPS (lpj=73984)
[    0.060000] pid_max: default: 32768 minimum: 301
[    0.060000] Mount-cache hash table entries: 512
[    0.070000] CPU: Testing write buffer coherency: ok
[    0.145000] S3C Power Management, Copyright 2004 Simtec Electronics
[    0.145000] S3C2410: Initialising architecture
[    0.250000] bio: create slab <bio-0> at 0
[    0.545000] Trying to unpack rootfs image as initramfs...
[    0.550000] rootfs image is not initramfs (junk in compressed archive); looks like an initrd
[    1.880000] Freeing initrd memory: 8192K
[    1.905000] msgmni has been set to 59
[    1.910000] io scheduler noop registered
[    1.910000] io scheduler deadline registered
[    1.910000] io scheduler cfq registered (default)
[    1.915000] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled
[    1.945000] s3c2410-uart.0: ttySAC0 at MMIO 0x50000000 (irq = 70) is a S3C2410
[    1.955000] console [ttySAC0] enabled
s3c24xx_serial_probe(c01f729c, c020253c) 1

s3c24xx_serial_probe: initialising port c02022c4...

s3c24xx_serial_init_port: port=c02022e8, platdev=c01f729c

s3c24xx_serial_init_port: c02022e8 (hw 1)...

resource c01f49f0 (50004000..50007fff)

port: map=50004000, mem=f7004000, irq=73 (73,74), clock=1

s3c2410_serial_resetport: port=c02022e8 (50004000), cfg=c0203b58

s3c24xx_serial_probe: adding port

[    1.965000] s3c2410-uart.1: ttySAC1 at MMIO 0x50004000 (irq = 73) is a S3C2410
s3c24xx_serial_probe(c01f7384, c020253c) 2

s3c24xx_serial_probe: initialising port c0202388...

s3c24xx_serial_init_port: port=c02023ac, platdev=c01f7384

s3c24xx_serial_init_port: c02023ac (hw 2)...

resource c01f4a28 (50008000..5000bfff)

port: map=50008000, mem=f7008000, irq=76 (76,77), clock=1

s3c2410_serial_resetport: port=c02023ac (50008000), cfg=c0203b78

s3c24xx_serial_probe: adding port

[    2.925000] s3c2410-uart.2: ttySAC2 at MMIO 0x50008000 (irq = 76) is a S3C2410
[    3.370000] brd: module loaded
[    3.370000] mousedev: PS/2 mouse device common for all mice
s3c24xx_serial_startup: port=50000000 (f7000000,c020244c)

requesting tx irq...

s3c24xx_serial_startup ok

config: 8bits/char

setting ulcon to 00000003, brddiv to 64, udivslot 00000000

uart: ulcon = 0x00000003, ucon = 0x00000385, ufcon = 0x00000011

[    3.375000] RAMDISK: ext2 filesystem found at block 0
[    3.375000] RAMDISK: Loading 2048KiB [1 disk] into ram disk... |/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\done.
[    4.365000] EXT2-fs (ram0): warning: mounting unchecked fs, running e2fsck is recommended
[    4.365000] VFS: Mounted root (ext2 filesystem) on device 1:0.
[    4.375000] Freeing init memory: 100K
config: 8bits/char

setting ulcon to 00000003, brddiv to 64, udivslot 00000000

uart: ulcon = 0x00000003, ucon = 0x00000385, ufcon = 0x00000011

'''
]
		return expected_output
