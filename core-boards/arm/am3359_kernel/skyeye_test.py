#coding:utf-8
import time
import os
import sys
from skyeye_autotest_command import *

class test():
	def __init__(self):
		testname="am3359.skyeye"
		test_branch='new_framework'
		enable=True
		run_system="all"
		self.ac=autotest_command(testname,enable,run_system,test_branch)

	def dev_init(self):
		self.ac.add_file_out_dev('uart0','log.txt')

	def test_flow(self):
		self.ac.run_script()
		self.dev_init()
		self.ac.run(10)
		self.ac.stop()
		self.ac.compare("log.txt",self.expect()[0],60)
		self.ac.reset()
		self.ac.output_result()
	def test_main(self):
		self.test_flow()
	def expect(self):
		expected_output=['''	
		
[    0.000000] Booting Linux on physical CPU 0x0

[    0.000000] Initializing cgroup subsys cpuset

[    0.000000] Initializing cgroup subsys cpu

[    0.000000] Initializing cgroup subsys cpuacct

[    0.000000] Linux version 4.1.6-g52c4aa7 (skx@skx-desktop) (gcc version 4.9.3 20150413 (prerelease) (Linaro GCC 4.9-2015.05) ) #495 PREEMPT Mon Jan 18 11:54:08 CST 2016

[    0.000000] CPU: ARMv7 Processor [413fc082] revision 2 (ARMv7), cr=10c5387d

[    0.000000] CPU: PIPT / VIPT nonaliasing data cache, VIPT nonaliasing instruction cache

[    0.000000] Machine model: TI AM335x EVM

[    0.000000] bootconsole [earlycon0] enabled

[    0.000000] cma: Reserved 24 MiB at 0x8e800000

[    0.000000] Memory policy: Data cache writeback

[    0.000000] CPU: All CPU(s) started in SVC mode.

[    0.000000] AM335X ES1.0 (neon )

[    0.000000] Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 64960

[    0.000000] Kernel command line: root=/dev/ram0 initrd=0x86400000,10M rw console=ttyO0,115200 earlyprintk

[    0.000000] PID hash table entries: 1024 (order: 0, 4096 bytes)

[    0.000000] Dentry cache hash table entries: 32768 (order: 5, 131072 bytes)

[    0.000000] Inode-cache hash table entries: 16384 (order: 4, 65536 bytes)

[    0.000000] Memory: 220836K/262144K available (2347K kernel code, 122K rwdata, 920K rodata, 168K init, 126K bss, 16732K reserved, 24576K cma-reserved, 0K highmem)

[    0.000000] Virtual kernel memory layout:

[    0.000000]     vector  : 0xffff0000 - 0xffff1000   (   4 kB)

[    0.000000]     fixmap  : 0xffc00000 - 0xfff00000   (3072 kB)

[    0.000000]     vmalloc : 0xd0800000 - 0xff000000   ( 744 MB)

[    0.000000]     lowmem  : 0xc0000000 - 0xd0000000   ( 256 MB)

[    0.000000]     pkmap   : 0xbfe00000 - 0xc0000000   (   2 MB)

[    0.000000]     modules : 0xbf000000 - 0xbfe00000   (  14 MB)

[    0.000000]       .text : 0xc0008000 - 0xc0339034   (3269 kB)

[    0.000000]       .init : 0xc033a000 - 0xc0364000   ( 168 kB)

[    0.000000]       .data : 0xc0364000 - 0xc0382b38   ( 123 kB)

[    0.000000]        .bss : 0xc0385000 - 0xc03a4908   ( 127 kB)

[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=1, Nodes=1

[    0.000000] Preemptible hierarchical RCU implementation.

[    0.000000] 	Additional per-CPU info printed with stalls.

[    0.000000] NR_IRQS:16 nr_irqs:16 16

[    0.000000] IRQ: Found an INTC at 0xfa200000 (revision 5.0) with 128 interrupts

[    0.000000] OMAP clockevent source: timer2 at 19200000 Hz

[    0.000000] sched_clock: 32 bits at 19MHz, resolution 52ns, wraps every 111848106981ns

[    0.000005] clocksource timer1: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 99544814920 ns

[    0.000005] OMAP clocksource: timer1 at 19200000 Hz

[    0.000010] Console: colour dummy device 80x30

[    0.000010] Calibrating delay loop... 

'''
]
		return expected_output



