Linux version 3.2.72 (skx@skx-desktop) (gcc version 4.4.3 (ctng-1.6.1) ) #109 PREEMPT Thu Dec 3 11:19:17 CST 2015
CPU: ARM920T [41129200] revision 0 (ARMv4T), cr=00003177
CPU: VIVT data cache, VIVT instruction cache
Machine: Atmel AT91RM9200-DK
Memory policy: ECC disabled, Data cache writeback
AT91: Detected soc type: at91rm9200
AT91: Detected soc subtype: Unknown
AT91: sram at 0x200000 of 0x4000 mapped at 0xfef74000
Clocks: CPU 178 MHz, master 59 MHz, main 18.432 MHz
Built 1 zonelists in Zone order, mobility grouping off.  Total pages: 4064
Kernel command line: root=/dev/ram0 initrd=0x00800000,0x00500000 rw console=ttyS0,115200
PID hash table entries: 64 (order: -4, 256 bytes)
Dentry cache hash table entries: 2048 (order: 1, 8192 bytes)
Inode-cache hash table entries: 1024 (order: 0, 4096 bytes)
Memory: 16MB = 16MB total
Memory: 8632k/8632k available, 7752k reserved, 0K highmem
Virtual kernel memory layout:
    vector  : 0xffff0000 - 0xffff1000   (   4 kB)
    fixmap  : 0xfff00000 - 0xfffe0000   ( 896 kB)
    vmalloc : 0xc1800000 - 0xfee00000   ( 982 MB)
    lowmem  : 0xc0000000 - 0xc1000000   (  16 MB)
    modules : 0xbf000000 - 0xc0000000   (  16 MB)
      .text : 0xc0008000 - 0xc02204f0   (2146 kB)
      .init : 0xc0221000 - 0xc023d000   ( 112 kB)
      .data : 0xc023e000 - 0xc0254100   (  89 kB)
       .bss : 0xc0254124 - 0xc0263e04   (  64 kB)
SLUB: Genslabs=13, HWalign=32, Order=0-3, MinObjects=0, CPUs=1, Nodes=1
NR_IRQS:192
AT91: 96 gpio irqs in 3 banks
Console: colour dummy device 80x30
Calibrating delay loop... 299.82 BogoMIPS (lpj=1499136)
pid_max: default: 32768 minimum: 301
Mount-cache hash table entries: 512
CPU: Testing write buffer coherency: ok
AT91: Power Management
bio: create slab <bio-0> at 0
Switching to clocksource 32k_counter
Trying to unpack rootfs image as initramfs...
rootfs image is not initramfs (junk in compressed archive); looks like an initrd
Freeing initrd memory: 5120K
NetWinder Floating Point Emulator V0.97 (double precision)
msgmni has been set to 26
io scheduler noop registered
io scheduler deadline registered (default)
atmel_usart.0: ttyS0 at MMIO 0xfffff200 (irq = 1) is a ATMEL_SERIAL
console [ttyS0] enabled
atmel_usart.2: ttyS1 at MMIO 0xfffc4000 (irq = 7) is a ATMEL_SERIAL
brd: module loaded
loop: module loaded
mousedev: PS/2 mouse device common for all mice
RAMDISK: ext2 filesystem found at block 0
RAMDISK: Loading 2048KiB [1 disk] into ram disk... |/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\done.
VFS: Mounted root (ext2 filesystem) on device 1:0.
Freeing init memory: 112K
ifconfig: socket: Function not implemented
ifconfig: socket: Function not implemented
Welcome to
     _      _____      __   __    _      _
    / \    /  __ \    /  \_/  \  | |    |_|                 
   / _ \   | |  | |  / /\   /\ \ | |     _ ____  _   _  _  _ 
  / /_\ \  | |__| | / /  \_/  \ \| |    | |  _ \| | | |\ \/ /
 / /___\ \ | |__\ \ | |       | || |___ | | |_| | |_| |/    \
/_/	\_\| |   \_\|_|       |_||_____||_|_| |_|\____|\_/\_/
	   
ARMLinux for Skyeye 
For further information please check:
http://www.skyeye.org/



BusyBox v1.00 (2004.12.27-02:03+0000) Built-in shell (ash)
Enter 'help' for a list of built-in commands.

/ # 