#coding:utf-8
import time
import os
import sys
from skyeye_autotest_command import *

class test():
	def __init__(self):
		testname="at91rm9200.skyeye"
		test_branch='new_framework'
		enable=True
		run_system="all"
		self.ac=autotest_command(testname,enable,run_system,test_branch)

	def dev_init(self):
		self.ac.add_file_out_dev('dbguart','log.txt')

	def test_flow(self):
		self.ac.run_script()
		self.dev_init()
		self.ac.run(40)
		self.ac.stop()
		self.ac.compare("log.txt",self.expect()[0],100)
		self.ac.reset()
		self.ac.output_result()
	def test_main(self):
		self.test_flow()
	def expect(self):
		expected_output=['''
Linux version 3.2.72 (skx@skx-desktop) (gcc version 4.4.3 (ctng-1.6.1) ) #109 PREEMPT Thu Dec 3 11:19:17 CST 2015
CPU: ARM920T [41129200] revision 0 (ARMv4T), cr=00003177
CPU: VIVT data cache, VIVT instruction cache
Machine: Atmel AT91RM9200-DK
Memory policy: ECC disabled, Data cache writeback
AT91: Detected soc type: at91rm9200
AT91: Detected soc subtype: Unknown
AT91: sram at 0x200000 of 0x4000 mapped at 0xfef74000
Clocks: CPU 178 MHz, master 59 MHz, main 18.432 MHz
Built 1 zonelists in Zone order, mobility grouping off.  Total pages: 4064
Kernel command line: root=/dev/ram0 initrd=0x00800000,0x00500000 rw console=ttyS0,115200
PID hash table entries: 64 (order: -4, 256 bytes)
Dentry cache hash table entries: 2048 (order: 1, 8192 bytes)
Inode-cache hash table entries: 1024 (order: 0, 4096 bytes)
Memory: 16MB = 16MB total
Memory: 8632k/8632k available, 7752k reserved, 0K highmem
Virtual kernel memory layout:
    vector  : 0xffff0000 - 0xffff1000   (   4 kB)
    fixmap  : 0xfff00000 - 0xfffe0000   ( 896 kB)
    vmalloc : 0xc1800000 - 0xfee00000   ( 982 MB)
    lowmem  : 0xc0000000 - 0xc1000000   (  16 MB)
    modules : 0xbf000000 - 0xc0000000   (  16 MB)
      .text : 0xc0008000 - 0xc02204f0   (2146 kB)
      .init : 0xc0221000 - 0xc023d000   ( 112 kB)
      .data : 0xc023e000 - 0xc0254100   (  89 kB)
       .bss : 0xc0254124 - 0xc0263e04   (  64 kB)
SLUB: Genslabs=13, HWalign=32, Order=0-3, MinObjects=0, CPUs=1, Nodes=1
NR_IRQS:192
AT91: 96 gpio irqs in 3 banks
Console: colour dummy device 80x30
'''
]
		return expected_output



