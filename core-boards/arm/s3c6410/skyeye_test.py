#coding:utf-8
import time
import os
import sys
from skyeye_autotest_command import *

class test():
	def __init__(self):
		testname="s3c6410.skyeye"
		test_branch='new_framework'
		enable=True
		run_system="all"
		self.ac=autotest_command(testname,enable,run_system,test_branch)

	def dev_init(self):
		self.ac.add_file_out_dev('core_0_uart_0','log.txt')

	def test_flow(self):
		self.ac.run_script()
		self.dev_init()
		self.ac.run(60)
		self.ac.stop()
		self.ac.compare("log.txt",self.expect()[0],99)
		self.ac.reset()
		self.ac.output_result()
	def test_main(self):
		self.test_flow()
	def expect(self):
		expected_output=['''
Linux version 2.6.34.14 (lyj@LYJ-WPC) (gcc version 4.2.1) #11 Mon Jul 29 13:55:33 CST 2013
CPU: ARMv6-compatible processor [0007b000] revision 0 (ARMv6TEJ), cr=00c5387f
CPU: VIPT nonaliasing data cache, VIPT nonaliasing instruction cache
Machine: SMDK6410
Warning: bad configuration page, trying to continue
Memory policy: ECC disabled, Data cache writeback
CPU S3C6410 (id 0x36410101)
S3C24XX Clocks, Copyright 2004 Simtec Electronics
camera: no parent clock specified
S3C64XX: PLL settings, A=400000000, M=133000000, E=97699996
S3C64XX: HCLK2=133000000, HCLK=133000000, PCLK=66500000
mout_apll: source is ext_xtal (0), rate is 12000000
mout_epll: source is ext_xtal (0), rate is 12000000
mout_mpll: source is ext_xtal (0), rate is 12000000
mmc_bus: source is mout_epll (0), rate is 12000000
mmc_bus: source is mout_epll (0), rate is 12000000
mmc_bus: source is mout_epll (0), rate is 12000000
usb-bus-host: source is clk_48m (0), rate is 48000000
uclk1: source is mout_epll (0), rate is 12000000
spi-bus: source is mout_epll (0), rate is 12000000
spi-bus: source is mout_epll (0), rate is 12000000
audio-bus: source is mout_epll (0), rate is 12000000
audio-bus: source is mout_epll (0), rate is 12000000
irda-bus: source is mout_epll (0), rate is 12000000
camera: no parent clock specified
Built 1 zonelists in Zone order, mobility grouping off.  Total pages: 4064
Kernel command line: console=ttySAC0,115200 init=/bin/sh root=/dev/ram initrd=0x50800000,3M
PID hash table entries: 64 (order: -4, 256 bytes)
Dentry cache hash table entries: 2048 (order: 1, 8192 bytes)
Inode-cache hash table entries: 1024 (order: 0, 4096 bytes)
Memory: 16MB = 16MB total
Memory: 11016k/11016k available, 5368k reserved, 0K highmem
Virtual kernel memory layout:
    vector  : 0xffff0000 - 0xffff1000   (   4 kB)
    fixmap  : 0xfff00000 - 0xfffe0000   ( 896 kB)
    DMA     : 0xffc00000 - 0xffe00000   (   2 MB)
    vmalloc : 0xc1800000 - 0xe0000000   ( 488 MB)
    lowmem  : 0xc0000000 - 0xc1000000   (  16 MB)
    modules : 0xbf000000 - 0xc0000000   (  16 MB)
      .init : 0xc0008000 - 0xc001d000   (  84 kB)
      .text : 0xc001d000 - 0xc01c7000   (1704 kB)
      .data : 0xc01c8000 - 0xc01e0d00   ( 100 kB)
SLUB: Genslabs=11, HWalign=32, Order=0-3, MinObjects=0, CPUs=1, Nodes=1
Hierarchical RCU implementation.
NR_IRQS:246
VIC @f4000000: id 0x00000000, vendor 0x00
VIC: unknown vendor, continuing anyways
VIC @f4010000: id 0x00000000, vendor 0x00
VIC: unknown vendor, continuing anyways
Console: colour dummy device 80x30
console [ttySAC0] enabled
Calibrating delay loop... 6.50 BogoMIPS (lpj=32512)
Mount-cache hash table entries: 512
CPU: Testing write buffer coherency: ok
s3c64xx_dma_init: Registering DMA channels
s3c64xx_dma_init1: registering DMA 0 (c1808100)
s3c64xx_dma_init1: registering DMA 1 (c1808120)
s3c64xx_dma_init1: registering DMA 2 (c1808140)
s3c64xx_dma_init1: registering DMA 3 (c1808160)
s3c64xx_dma_init1: registering DMA 4 (c1808180)
s3c64xx_dma_init1: registering DMA 5 (c18081a0)
s3c64xx_dma_init1: registering DMA 6 (c18081c0)
s3c64xx_dma_init1: registering DMA 7 (c18081e0)
PL080: IRQ 73, at c1808000
s3c64xx_dma_init1: registering DMA 8 (c180c100)
s3c64xx_dma_init1: registering DMA 9 (c180c120)
s3c64xx_dma_init1: registering DMA 10 (c180c140)
s3c64xx_dma_init1: registering DMA 11 (c180c160)
s3c64xx_dma_init1: registering DMA 12 (c180c180)
s3c64xx_dma_init1: registering DMA 13 (c180c1a0)
s3c64xx_dma_init1: registering DMA 14 (c180c1c0)
s3c64xx_dma_init1: registering DMA 15 (c180c1e0)
PL080: IRQ 74, at c180c000
S3C6410: Initialising architecture
bio: create slab <bio-0> at 0
Trying to unpack rootfs image as initramfs...
rootfs image is not initramfs (junk in compressed archive); looks like an initrd
Freeing initrd memory: 3072K
NetWinder Floating Point Emulator V0.97 (double precision)
ROMFS MTD (C) 2007 Red Hat, Inc.
io scheduler noop registered
io scheduler deadline registered
io scheduler cfq registered (default)
Serial: 8250/16550 driver, 4 ports, IRQ sharing disabled
s3c6400-uart.0: s3c2410_serial0 at MMIO 0x7f005000 (irq = 16) is a S3C6400/10
s3c6400-uart.1: s3c2410_serial1 at MMIO 0x7f005400 (irq = 20) is a S3C6400/10
s3c6400-uart.2: s3c2410_serial2 at MMIO 0x7f005800 (irq = 24) is a S3C6400/10
s3c6400-uart.3: s3c2410_serial3 at MMIO 0x7f005c00 (irq = 28) is a S3C6400/10
brd: module loaded
loop: module loaded
mice: PS/2 mouse device common for all mice
RAMDISK: ext2 filesystem found at block 0
RAMDISK: Loading 2048KiB [1 disk] into ram disk... |/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\done.
VFS: Mounted root (ext2 filesystem) readonly on device 1:0.
Freeing init memory: 84K


BusyBox v1.00 (2004.12.27-02:03+0000) Built-in shell (ash)
Enter 'help' for a list of built-in commands.

/bin/sh: can't access tty; job control turned off
/ #
'''
]
		return expected_output
