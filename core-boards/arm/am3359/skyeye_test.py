#coding:utf-8
import time
import os
import sys
from skyeye_autotest_command import *

class test():
	def __init__(self):
		testname="am3359.skyeye"
		test_branch='new_framework'
		enable=True
		run_system="all"
		self.ac=autotest_command(testname,enable,run_system,test_branch)

	def dev_init(self):
		self.ac.add_file_out_dev('core_0_uart_0','log.txt')

	def test_flow(self):
		self.ac.run_script()
		self.dev_init()
		self.ac.run(4)
		self.ac.stop()
		self.ac.compare("log.txt",self.expect()[0],100)
		self.ac.reset()
		self.ac.output_result()
	def test_main(self):
		self.test_flow()
	def expect(self):
		expected_output=['''
5eb63bbbe01eeed093cb22bb8f5acdc3
'''
]
		return expected_output



